import React, {Component} from 'react';

import "./index.css"

type State = {
  username: string
  password: string
  isRegister: boolean
  status: string
}

type Props = {
  setToken: any
};

class Login extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isRegister: false,
      username: "",
      password: "",
      status: "Not logged in"
    }
  }

  componentDidUpdate() {
  }

  login = (register?: boolean) => {
    fetch(`http://${process.env.REACT_APP_API_ENDPOINT}/${register ? 'register' : 'login'}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      }),
    }).then(response => response.json())
      .then(data => {
        this.setState({status: data.error || data.status})

        if (data.token) {
          window.localStorage.setItem("token", data.token)
          window.localStorage.setItem("username", this.state.username)

          this.props.setToken(data.token)
        }
      })
      .catch(error => console.log(error));

  }

  render() {
    return (
      <div className={"login"}>
        <input type="text" onChange={event => this.setState({username: event.target.value})}/>
        <input type="password" onChange={event => this.setState({password: event.target.value})}/>
        <div className={"controll"}>
          <button
            onClick={() => this.login()}>
            Login
          </button>
          <button
            onClick={() => this.login(true)}>
            Register
          </button>
        </div>
        <div className="response">
          {this.state.status}
        </div>
      </div>
    );
  }
}

export default Login;