import React, {Component} from 'react';
import {Coupon} from "../../Components/coupon";
import './index.css';

type CouponT = {
  "title": string
  "doc": string
  "description": string
  "coupon_code": string
}


type Props = {
  token: string
  setToken: any
};
type State = {
  coupons: CouponT[]
  coupon: CouponT
  modal: boolean
  file: File | undefined
  error: string
};


class Dashboard extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      file: undefined,
      coupons: [],
      coupon: {
        title: "",
        description: "",
        coupon_code: "",
        doc: "",
      },
      modal: false,
      error: ''
    };
    // setTimeout(this.getCoupons, 1000)
  }

  getCoupons = () => {
    fetch(`http://${process.env.REACT_APP_API_ENDPOINT}/coupons`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'token': this.props.token
      }, redirect: 'follow'
    })
      .then(response => response.json())
      .then(respose => this.setState({coupons: respose}))
      .catch(error => console.log('error', error));
  }


  addCoupon = () => {
    if (this.state.file) {
      var reader = new FileReader();
      reader.readAsDataURL(this.state.file);
      reader.onload = () => {
        fetch(`http://${process.env.REACT_APP_API_ENDPOINT}/coupons`, {
          method: 'POST',
          headers: {
            "token": this.props.token,
            "Content-Type": "application/json"
          },
          body: JSON.stringify({...this.state.coupon, doc: reader.result}),
          redirect: 'follow'
        })
          .then(response => response.json())
          .then(result => {
            result.error ?
              this.setState({error: result.error}) :
              this.setState({coupons: result})
          })
          .catch(error => console.log('error', error));
        this.setState({modal: false})
      };
    }
  }

  componentDidMount() {
    this.getCoupons()
  }

  componentDidUpdate() {
    console.log(this.state)

  }

  render() {
    return (
      <div className={"dashboard"}>
        {this.state.modal ? <div className="modal">
          <div className="container">
            <div className={"header"}>
              <span
                onClick={() => this.setState({modal: false})}
                style={{cursor: "pointer"}}>X</span>
            </div>
            <div className={"wrapper"}>
              <input type="text" placeholder={"Title"}
                     onChange={(evt => this.setState({
                       coupon: {
                         ...this.state.coupon,
                         title: evt.target.value
                       }
                     }))}/>
              <input type="text" placeholder={"Description"}
                     onChange={(evt => this.setState({
                       coupon: {
                         ...this.state.coupon,
                         description: evt.target.value
                       }
                     }))}/>
              <input type="text" placeholder={"Code"}
                     onChange={(evt => this.setState({
                       coupon: {
                         ...this.state.coupon,
                         coupon_code: evt.target.value
                       }
                     }))}/>
              <input type="file"
                     required
                     onChange={(evt => this.setState({
                       file: evt.target.files ? evt.target.files[0] : undefined

                     }))}/>
              <button onClick={this.addCoupon}>Add</button>
            </div>
          </div>
        </div> : null}
        <div className={"header"}>
          <span>{window.localStorage.getItem("username")}</span>
          <button onClick={() => {
            window.localStorage.removeItem("token")
            this.props.setToken("")
          }}>Exit
          </button>
        </div>
        <div className={"dashboard_grid"}>
          {this.state.coupons ? this.state.coupons.map((el, idx) => <Coupon coupon={el} key={idx}/>) : null}
          <div onClick={() => {
            this.setState({modal: true})
          }} className={"coupon"} style={{cursor: "pointer"}}>
            <h1>Add</h1>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;