// @flow
import * as React from 'react';
import './index.css';


type CouponT = {
  "title": string
  "doc": string
  "description": string
  "coupon_code": string
}

type Props = {
  coupon: CouponT
};
type State = {};

export class Coupon extends React.Component<Props, State> {
  download = () => {
    window.open(this.props.coupon.doc);
  }

  componentDidMount() {
    console.log()

  }

  render() {
    return (
      <div className={"coupon"}>
        <h1>{this.props.coupon.title}</h1>
        <p>{this.props.coupon.coupon_code}</p>
        <p>{this.props.coupon.description}</p>
        {/*<button onClick={this.download}>Download</button>*/}
        <a download={`${this.props.coupon.title}.${this.props.coupon.doc.substring(this.props.coupon.doc.indexOf('/') + 1, this.props.coupon.doc.indexOf(';base64'))}`} href={this.props.coupon.doc}>Download</a>
      </div>

    );
  }
  ;
}
;