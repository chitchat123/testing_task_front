import React, {useState} from 'react';
import './App.css';
import {Dashboard, Login} from './Screens';


function App() {
  const [token, setToken] = useState(window.localStorage.getItem("token") || "")
  return (
    <div className="App">
      {
        token ?
          <Dashboard token={token} setToken={setToken}/> :
          <Login setToken={setToken}/>
      }
    </div>
  );
}

export default App;
