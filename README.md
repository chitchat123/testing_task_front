### `yarn install`

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `.env`
Change `REACT_APP_API_ENDPOINT` to url of backend `(localhost:8000)`